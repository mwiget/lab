<?php
header("Content-type: text/plain");
$mac = explode(":",$_GET[mac]);
$hostname="apu$mac[4]$mac[5]";
?>
#!ipxe
set base-url http://<?php echo $_SERVER['SERVER_ADDR'] ?>

set hostname <?php echo $hostname ?>

kernel ${base-url}/fedora-coreos-31.20200407.3.0-live-kernel-x86_64 coreos.inst.install_dev=/dev/sda coreos.inst.stream=stable coreos.inst.ignition_url=${base-url}/${hostname}.ign console=ttyS0,115200 console=tty0
initrd ${base-url}/fedora-coreos-31.20200407.3.0-live-initramfs.x86_64.img
boot
