#!/bin/bash
user=mwiget
echo "pre-create folders and set their context to svirt_sandbox_file_t ..."
for apu in apu4a18 apu91dc apu49cc apu9290 apu4a7c; do
	echo -n "$apu: "
        ssh-keygen -f "/home/pi/.ssh/known_hosts" -R "$apu" >/dev/null 2>/dev/null
        ssh -q -o StrictHostKeyChecking=no $user@$apu "for i in {/var/lib/etcd,/etc/kubernetes/pki,/etc/kubernetes/pki/etcd,/etc/cni/net.d}; do sudo mkdir -p \$i && sudo chcon -Rt svirt_sandbox_file_t \$i; done" # ; sudo rm -f /etc/cni/net.d/87-podman-bridge.conflist"
	echo "symlink from /usr/lib/exec/cni to /opt/cni/bin to keep kubernetes happy ..."
        ssh -q -o StrictHostKeyChecking=no $user@$apu "sudo mkdir /opt/cni &&  sudo ln -s /usr/libexec/cni /opt/cni/bin"
	echo "enabling kubelet ..."
        ssh -q -o StrictHostKeyChecking=no $user@$apu sudo systemctl enable --now kubelet
	echo "enabling docker ..."
        ssh -q -o StrictHostKeyChecking=no $user@$apu sudo systemctl enable --now docker
        echo "done"
done
