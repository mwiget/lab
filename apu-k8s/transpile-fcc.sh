#!/bin/sh

for file in *.fcc; do
  target="${file%%.*}.ign"
  echo transpiling $file to $target ...
  fcct --pretty --strict < $file > /var/www/html/$target
  #docker run -i --rm quay.io/coreos/fcct:release --pretty --strict < $file > ignition/$target
done
ls -l /var/www/html/*.ign
