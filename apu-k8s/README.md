# Kubernetes with Calico on APU cluster

Maybe not a great idea trying to bring up kubernetes on FCOS after all, see current discussion https://github.com/coreos/fedora-coreos-tracker/issues/93, opened in Dec 2018 and still open by May 2020.


## Preparation

Clean install of the cluster nodes by iPXE booting all nodes. This can be triggered with the [wipe.sh](wipe.sh) script.
The device use iPXE boot to load Fedora CoreOS with ignition files prepared for each host.

Wait for all nodes to be fully up and running with the kubadmin/kubectl etc packages installed (requires 2 reboots). Check the status with the following script:

```
pi@ipxe:~/lab/apu-k8s $ ./show-version.sh 
apu4a18: Linux apu4a18 5.5.17-200.fc31.x86_64 #1 SMP Mon Apr 13 15:29:42 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
kubeadmv1.18.2
apu91dc: Linux apu91dc 5.5.17-200.fc31.x86_64 #1 SMP Mon Apr 13 15:29:42 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
kubeadmv1.18.2
apu49cc: Linux apu49cc 5.5.17-200.fc31.x86_64 #1 SMP Mon Apr 13 15:29:42 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
kubeadmv1.18.2
apu9290: Linux apu9290 5.5.17-200.fc31.x86_64 #1 SMP Mon Apr 13 15:29:42 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
kubeadmv1.18.2
apu4a7c: Linux apu4a7c 5.5.17-200.fc31.x86_64 #1 SMP Mon Apr 13 15:29:42 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
kubeadmv1.18.2
pi@ipxe:~/lab/apu-k8s $ 
```

Followed the [https://docs.projectcalico.org/getting-started/kubernetes/quickstart](https://docs.projectcalico.org/getting-started/kubernetes/quickstart), but needed to make several adjustments, thanks to the immutal nature of Fedora CoreOS.

TODO: need to document my changes ... 


Prep, install k8s master and worker nodes and Calico using the script [install-all.sh](./install-all.sh).

Verify successful installation:

```
pi@ipxe:~/lab/apu-k8s $ ./show-k8s.sh 
+ ssh -q -o StrictHostKeyChecking=no mwiget@apu4a18 'kubectl get nodes && kubectl get pods --all-namespaces'
NAME      STATUS   ROLES    AGE   VERSION
apu49cc   Ready    <none>   35m   v1.18.2
apu4a18   Ready    master   35m   v1.18.2
apu4a7c   Ready    <none>   34m   v1.18.2
apu91dc   Ready    <none>   22m   v1.18.2
apu9290   Ready    <none>   34m   v1.18.2
NAMESPACE     NAME                                       READY   STATUS             RESTARTS   AGE
kube-system   calico-kube-controllers-789f6df884-7vbmp   1/1     Running            0          34m
kube-system   calico-node-7cpkb                          1/1     Running            0          34m
kube-system   calico-node-kmcgb                          0/1     Running            0          34m
kube-system   calico-node-vs4rl                          0/1     CrashLoopBackOff   11         34m
kube-system   calico-node-vwxrb                          1/1     Running            0          22m
kube-system   calico-node-xsmbs                          1/1     Running            0          34m
kube-system   coredns-66bff467f8-5jlw6                   1/1     Running            0          35m
kube-system   coredns-66bff467f8-z5t75                   1/1     Running            0          35m
kube-system   etcd-apu4a18                               1/1     Running            0          35m
kube-system   kube-apiserver-apu4a18                     1/1     Running            0          35m
kube-system   kube-controller-manager-apu4a18            1/1     Running            0          35m
kube-system   kube-proxy-6g7ll                           1/1     Running            0          35m
kube-system   kube-proxy-jgzw7                           1/1     Running            0          34m
kube-system   kube-proxy-kjnxw                           1/1     Running            0          35m
kube-system   kube-proxy-kvptt                           1/1     Running            0          34m
kube-system   kube-proxy-sld5r                           1/1     Running            0          22m
kube-system   kube-scheduler-apu4a18                     1/1     Running            0          35m
```

## Problems encountered

### 

```
mwiget@apu4a18 ~]$ kubectl get nodes
NAME      STATUS   ROLES    AGE     VERSION
apu49cc   Ready    <none>   20m     v1.18.2
apu4a18   Ready    master   21m     v1.18.2
apu4a7c   Ready    <none>   20m     v1.18.2
apu91dc   Ready    <none>   7m34s   v1.18.2
apu9290   Ready    <none>   20m     v1.18.2
[mwiget@apu4a18 ~]$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                       READY   STATUS             RESTARTS   AGE
kube-system   calico-kube-controllers-789f6df884-7vbmp   1/1     Running            0          19m
kube-system   calico-node-7cpkb                          1/1     Running            0          19m
kube-system   calico-node-kmcgb                          0/1     Running            0          19m
kube-system   calico-node-vs4rl                          0/1     CrashLoopBackOff   8          19m
kube-system   calico-node-vwxrb                          1/1     Running            0          7m36s
kube-system   calico-node-xsmbs                          1/1     Running            0          19m
kube-system   coredns-66bff467f8-5jlw6                   1/1     Running            0          20m
kube-system   coredns-66bff467f8-z5t75                   1/1     Running            0          20m
kube-system   etcd-apu4a18                               1/1     Running            0          20m
kube-system   kube-apiserver-apu4a18                     1/1     Running            0          20m
kube-system   kube-controller-manager-apu4a18            1/1     Running            0          20m
kube-system   kube-proxy-6g7ll                           1/1     Running            0          20m
kube-system   kube-proxy-jgzw7                           1/1     Running            0          20m
kube-system   kube-proxy-kjnxw                           1/1     Running            0          20m
kube-system   kube-proxy-kvptt                           1/1     Running            0          20m
kube-system   kube-proxy-sld5r                           1/1     Running            0          7m36s
kube-system   kube-scheduler-apu4a18                     1/1     Running            0          20m
```

The pod calico-node in CrashLoopBackOff is trying to start on the master node, which it isn't allowed yet. Need to taint the master with the following command:

```
[mwiget@apu4a18 ~]$ kubectl taint nodes --all node-role.kubernetes.io/master-
node/apu4a18 untainted
taint "node-role.kubernetes.io/master" not found
taint "node-role.kubernetes.io/master" not found
taint "node-role.kubernetes.io/master" not found
taint "node-role.kubernetes.io/master" not found
```

And now all is well, but not for long ...

```
mwiget@apu4a18 ~]$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE
kube-system   calico-kube-controllers-789f6df884-7vbmp   1/1     Running   0          22m
kube-system   calico-node-7cpkb                          1/1     Running   0          22m
kube-system   calico-node-kmcgb                          0/1     Running   0          22m
kube-system   calico-node-vs4rl                          0/1     Running   9          22m
kube-system   calico-node-vwxrb                          1/1     Running   0          10m
kube-system   calico-node-xsmbs                          1/1     Running   0          22m
kube-system   coredns-66bff467f8-5jlw6                   1/1     Running   0          23m
kube-system   coredns-66bff467f8-z5t75                   1/1     Running   0          23m
kube-system   etcd-apu4a18                               1/1     Running   0          23m
kube-system   kube-apiserver-apu4a18                     1/1     Running   0          23m
kube-system   kube-controller-manager-apu4a18            1/1     Running   0          23m
kube-system   kube-proxy-6g7ll                           1/1     Running   0          23m
kube-system   kube-proxy-jgzw7                           1/1     Running   0          23m
kube-system   kube-proxy-kjnxw                           1/1     Running   0          23m
kube-system   kube-proxy-kvptt                           1/1     Running   0          22m
kube-system   kube-proxy-sld5r                           1/1     Running   0          10m
kube-system   kube-scheduler-apu4a18                     1/1     Running   0          23m
```

```
mwiget@apu4a18 ~]$ kubectl logs calico-node-vs4rl -n kube-system
2020-05-08 11:49:51.322 [INFO][9] startup/startup.go 299: Early log level set to info
2020-05-08 11:49:51.323 [INFO][9] startup/startup.go 315: Using NODENAME environment for node name
2020-05-08 11:49:51.323 [INFO][9] startup/startup.go 327: Determined node name: apu4a18
2020-05-08 11:49:51.334 [INFO][9] startup/startup.go 359: Checking datastore connection
2020-05-08 11:49:51.374 [INFO][9] startup/startup.go 383: Datastore connection verified
2020-05-08 11:49:51.374 [INFO][9] startup/startup.go 104: Datastore is ready
2020-05-08 11:49:51.456 [INFO][9] startup/startup.go 425: Initialize BGP data
2020-05-08 11:49:51.461 [INFO][9] startup/startup.go 664: Using autodetected IPv4 address on interface cni-podman0: 10.88.0.1/16
2020-05-08 11:49:51.461 [INFO][9] startup/startup.go 495: Node IPv4 changed, will check for conflicts
2020-05-08 11:49:51.489 [WARNING][9] startup/startup.go 1010: Calico node 'apu9290' is already using the IPv4 address 10.88.0.1.
2020-05-08 11:49:51.489 [INFO][9] startup/startup.go 263: Clearing out-of-date IPv4 address from this node IP="10.88.0.1/16"
2020-05-08 11:49:51.527 [WARNING][9] startup/startup.go 1214: Terminating
Calico node failed to start
```

```
[mwiget@apu4a18 ~]$ ifconfig cni-podman0
cni-podman0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.88.0.1  netmask 255.255.0.0  broadcast 10.88.255.255
        inet6 fe80::bc74:4ff:feb2:b538  prefixlen 64  scopeid 0x20<link>
        ether be:74:04:b2:b5:38  txqueuelen 1000  (Ethernet)
        RX packets 10420  bytes 716032 (699.2 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 10734  bytes 3817656 (3.6 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

```

Reboot all nodes didn't help. 

Somehow there must be a conflict with calico node apu9290 using the same IP ...

Yep:

```
mwiget@apu9290 ~]$ ifconfig cni-podman0
cni-podman0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.88.0.1  netmask 255.255.0.0  broadcast 10.88.255.255
        inet6 fe80::bcb0:3eff:feb6:34ff  prefixlen 64  scopeid 0x20<link>
        ether be:b0:3e:b6:34:ff  txqueuelen 1000  (Ethernet)
        RX packets 165  bytes 13959 (13.6 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 198  bytes 191214 (186.7 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

```

Maybe because I added that node much later. Trying to wipe everything again..


Adding calicoctl:

```
[mwiget@apu4a18 ~]$ kubectl apply -f https://docs.projectcalico.org/manifests/calicoctl.yaml
```

```
[mwiget@apu4a18 ~]$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                       READY   STATUS             RESTARTS   AGE
kube-system   calico-kube-controllers-789f6df884-rz48l   1/1     Running            1          22m
kube-system   calico-node-7pngz                          1/1     Running            0          22m
kube-system   calico-node-9mggb                          0/1     Running            1          22m
kube-system   calico-node-cl9tn                          0/1     CrashLoopBackOff   8          22m
kube-system   calico-node-hnl2j                          1/1     Running            0          22m
kube-system   calico-node-lkfb2                          1/1     Running            0          22m
kube-system   calicoctl                                  1/1     Running            0          64s
kube-system   coredns-66bff467f8-d9hnn                   1/1     Running            0          23m
kube-system   coredns-66bff467f8-ww464                   1/1     Running            0          23m
kube-system   etcd-apu4a18                               1/1     Running            0          23m
kube-system   kube-apiserver-apu4a18                     1/1     Running            0          23m
kube-system   kube-controller-manager-apu4a18            1/1     Running            0          23m
kube-system   kube-proxy-9cxsb                           1/1     Running            0          22m
kube-system   kube-proxy-f46t8                           1/1     Running            0          23m
kube-system   kube-proxy-hr9nv                           1/1     Running            1          22m
kube-system   kube-proxy-sx5m2                           1/1     Running            0          22m
kube-system   kube-proxy-vm5hh                           1/1     Running            0          23m
kube-system   kube-scheduler-apu4a18                     1/1     Running            0          23m
[mwiget@apu4a18 ~]$ 
```

Turns out, this might be expected behavior for redundany reasons. After a few re-installs, I see the crashing calico-node moving to other worker nodes, together with the existence of a cni-podman0 interface and the same IP address.


```
[mwiget@apu4a18 ~]$ kubectl get nodes -o wide
NAME      STATUS   ROLES    AGE   VERSION   INTERNAL-IP    EXTERNAL-IP   OS-IMAGE                        KERNEL-VERSION           CONTAINER-RUNTIME
apu49cc   Ready    <none>   54m   v1.18.2   192.168.0.30   <none>        Fedora CoreOS 31.20200420.3.0   5.5.17-200.fc31.x86_64   docker://18.9.8
apu4a18   Ready    master   55m   v1.18.2   192.168.0.32   <none>        Fedora CoreOS 31.20200420.3.0   5.5.17-200.fc31.x86_64   docker://18.9.8
apu4a7c   Ready    <none>   54m   v1.18.2   192.168.0.29   <none>        Fedora CoreOS 31.20200420.3.0   5.5.17-200.fc31.x86_64   docker://18.9.8
apu91dc   Ready    <none>   55m   v1.18.2   192.168.0.28   <none>        Fedora CoreOS 31.20200420.3.0   5.5.17-200.fc31.x86_64   docker://18.9.8
apu9290   Ready    <none>   54m   v1.18.2   192.168.0.26   <none>        Fedora CoreOS 31.20200420.3.0   5.5.17-200.fc31.x86_64   docker://18.9.8
```

Install calicocli:

```
$ kubectl apply -f https://docs.projectcalico.org/manifests/calicoctl.yaml
$ alias calicoctl="kubectl exec -i -n kube-system calicoctl -- /calicoctl"
```

Show calicocli help:

```
[mwiget@apu4a18 ~]$ calicoctl --help
Usage:
  calicoctl [options] <command> [<args>...]
    create    Create a resource by filename or stdin.
    replace   Replace a resource by filename or stdin.
    apply     Apply a resource by filename or stdin.  This creates a resource
              if it does not exist, and replaces a resource if it does exists.
    patch     Patch a pre-exisiting resource in place.
    delete    Delete a resource identified by file, stdin or resource type and
              name.
    get       Get a resource identified by file, stdin or resource type and
              name.
    label     Add or update labels of resources.
    convert   Convert config files between different API versions.
    ipam      IP address management.
    node      Calico node management.
    version   Display the version of calicoctl.
Options:
  -h --help               Show this screen.
  -l --log-level=<level>  Set the log level (one of panic, fatal, error,
                          warn, info, debug) [default: panic]
Description:
  The calicoctl command line tool is used to manage Calico network and security
  policy, to view and manage endpoint configuration, and to manage a Calico
  node instance.
  See 'calicoctl <command> --help' to read about a specific subcommand.
```
