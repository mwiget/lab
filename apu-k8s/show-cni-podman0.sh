#!/bin/bash
user=mwiget
for apu in apu4a18 apu91dc apu49cc apu9290 apu4a7c; do
	echo -n "$apu: "
        ssh -q -o StrictHostKeyChecking=no $user@$apu "ifconfig cni-podman0"
done
