#!/bin/bash
user=mwiget
apu=apu4a18

echo "install Calico ..."
scp -q -o StrictHostKeyChecking=no calico.yaml $user@$apu:
ssh -q -o StrictHostKeyChecking=no $user@$apu kubectl apply -f calico.yaml
#ssh -q -o StrictHostKeyChecking=no $user@$apu kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
echo "monitor progress with the following command on $apu:"
echo "watch kubectl get pods --all-namespaces"
exit 0
