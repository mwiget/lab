#!/bin/bash
user=mwiget
for apu in apu4a18 apu91dc apu49cc apu9290 apu4a7c; do
        echo rebooting $apu ...
        ssh -q -o StrictHostKeyChecking=no $user@$apu sudo reboot
done
