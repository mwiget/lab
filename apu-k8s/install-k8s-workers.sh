#!/bin/bash
user=mwiget
apu=apu4a18

echo "joining nodes to $apu via kubeadm: " 

joincmd=$(ssh -q -o StrictHostKeyChecking=no $user@$apu kubeadm token create --print-join-command 2>/dev/null)
echo $joincmd

echo ""
echo "creating join.cfg config from join command line ..."

#kubeadm join 192.168.0.32:6443 --token s91y5h.1ah5paoxm8ktd5sr --discovery-token-ca-cert-hash sha256:ff482b625324f78227aed67e3df02c299dfb871e65ac9569d9b5e574455e723d
# create-hash.sh would be an alternate method to create the discovery-token-ca-cert-hash

masteripport=$(echo $joincmd | cut -d' ' -f3)
TOKEN=$(echo $joincmd | cut -d' ' -f5)
CERT_HASH=$(echo $joincmd | cut -d' ' -f7)

cat > join.cfg <<EOF
apiVersion: kubeadm.k8s.io/v1beta2
kind: JoinConfiguration
discovery:
  bootstrapToken:
    apiServerEndpoint: $masteripport
    caCertHashes:
    - $CERT_HASH
    token: $TOKEN
nodeRegistration:
  kubeletExtraArgs:
    volume-plugin-dir: "/opt/libexec/kubernetes/kubelet-plugins/volume/exec/"
EOF

echo "joining cluster ..."
for node in apu9290 apu91dc apu49cc apu4a7c; do
	echo -n "$node joining cluster: ... "
	scp -o StrictHostKeyChecking=no join.cfg $user@$node: 2>/dev/null
        ssh -q -o StrictHostKeyChecking=no $user@$node sudo kubeadm join --config join.cfg 
done
rm join.cfg
echo "done"
