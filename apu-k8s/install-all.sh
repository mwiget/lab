#!/bin/sh
set -x
./prep-k8s-nodes.sh
./install-k8s-master.sh
./install-k8s-workers.sh
./install-calico.sh
