#!/bin/bash
user=mwiget
apu=apu4a18

echo ""
echo "copying kubeadm-master-node.cfg to $apu ..."
scp -o StrictHostKeyChecking=no kubeadm-master-node.cfg $user@$apu: 2>/dev/null

echo "kubeadm init on $apu ..."
ssh -q -o StrictHostKeyChecking=no $user@$apu sudo kubeadm init --config kubeadm-master-node.cfg

echo "creating $user/.kube/config ..."
ssh -q -o StrictHostKeyChecking=no $user@$apu "mkdir -p \$HOME/.kube && sudo cp -i /etc/kubernetes/admin.conf \$HOME/.kube/config && sudo chown \$(id -u):\$(id -g) \$HOME/.kube/config"

echo "allow pods on master node (required for calico-node) ..."
ssh -q -o StrictHostKeyChecking=no $user@$apu "kubectl taint nodes --all node-role.kubernetes.io/master-"

