#!/bin/bash
user=mwiget
master=apu4a18

set -x
ssh -q -o StrictHostKeyChecking=no $user@$master "kubectl get nodes && kubectl get pods --all-namespaces"
