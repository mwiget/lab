- use systemd to launch docker-compose
- but secrets file .env must exist first
- upload .env, then reboot node or restart service

docker-compose.yml isn't used. It is here as a reference example.

The shell scripts launch.sh and remove.sh can be used for local testing only. Their content
is added to systemd files as part of CoreOS ignition config.

