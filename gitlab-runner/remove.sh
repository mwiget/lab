#!/bin/bash

echo unregister gitlab-runner ...
source .env
docker run --rm -v gitlab-runner-volume:/etc/gitlab-runner gitlab/gitlab-runner unregister -n ${CI_RUNNER_DESCRIPTION} -u ${CI_SERVER_URL} --all-runners
docker stop gitlab-runner
docker volume remove gitlab-runner-volume
