#!/bin/bash

if [[ ! -e .env ]]; then
  echo "please create .env file first"
  exit 1
fi

source .env
docker pull gitlab/gitlab-runner

echo "unregister gitlab-runner or create a fresh gitlab-runner-volume ..."
docker run --rm -v gitlab-runner-volume:/etc/gitlab-runner gitlab/gitlab-runner unregister -n ${CI_RUNNER_DESCRIPTION} -u ${CI_SERVER_URL} --all-runners

echo "registering gitlab-runner $CI_RUNNER_DESCRIPTION, tag-list=$CI_RUNNER_TAGS ..."
docker run --rm --env-file .env -v gitlab-runner-volume:/etc/gitlab-runner gitlab/gitlab-runner register --non-interactive --executor=docker --docker-image=docker:stable --name=$CI_RUNNER_DESCRIPTION --tag-list=$CI_RUNNER_TAGS --locked=false --access-level=not_protected --docker-network-mode=host

echo "launching gitlab-runner ..."
docker run --rm -d --name gitlab-runner --env-file .env -v gitlab-runner-volume:/etc/gitlab-runner -v /var/run/docker.sock:/var/runn/docker.sock gitlab/gitlab-runner
