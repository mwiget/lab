#!/bin/bash
SERVER=192.168.0.53
echo "uploading .env file to register with gitlab.com/mwdca/fabric ..."
scp .env core@$SERVER:/etc/gitlab-runner/
echo "restart service gitlab-runner.service ..."
ssh core@$SERVER sudo systemctl restart gitlab-runner.service
