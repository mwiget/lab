<?php
header("Content-type: text/plain");
$mac = explode(":",$_GET[mac]);
$hostname="apu$mac[4]$mac[5]";
?>
#!ipxe
set my-url http://<?php echo $_SERVER['SERVER_ADDR'] ?>

set hostname <?php echo $hostname ?>

set mirror http://mirror.init7.net/fedora/fedora/linux/releases/32/Server/x86_64/os/

kernel ${mirror}/images/pxeboot/vmlinuz initrd=initrd.img repo=${mirror} console=ttyS0,115200
initrd ${mirror}/images/pxeboot/initrd.img
boot
