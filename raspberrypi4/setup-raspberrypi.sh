#!/bin/bash

sudo apt update
sudo apt -y upgrade

echo "installing various packages ..."
sudo apt -y install vim-nox dnsmasq pxelinux mosh \
  git screen make nginx python3-pip python3-pil python3-numpy tcpdump tmux cmake \
  apt-transport-https ca-certificates curl gnupg2 software-properties-common \
  manpages-dev build-essential samba samba-common-bin nmap RPi.GPIO clang  llvm \
  raspberrypi-kernel-headers bridge-utils

sudo pip3 install adafruit-circuitpython-rgb-display

echo "installing docker ce ..."
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh ./get-docker.sh

#curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
#sudo add-apt-repository \
#     "deb [arch=armhf] https://download.docker.com/linux/raspbian stretch edge"
#sudo apt update
#sudo apt -y --no-install-recommends install docker-ce docker-ce-cli containerd.io docker-compose

sudo pip3 install docker-compose

sudo pip3 install adafruit-circuitpython-rgb-display
sudo pip3 install --upgrade --force-reinstall spidev
sudo pip3 install adafruit-blinka
sudo apt-get install -y ttf-dejavu python3-pil python3-numpy
sudo apt -y autoremove

sudo usermod -aG docker $USER
newgrp docker
echo ""
docker version
echo ""

exit

echo "enable tmux mouse support ..."
echo "set -g mouse on" > ~/.tmux.conf

docker pull alpine
docker pull ubuntu

echo "install golang ..."
wget https://dl.google.com/go/go1.13.7.linux-armv6l.tar.gz
sudo tar -C /usr/local -xzf go1*.linux-armv6l.tar.gz

echo "installing visual studio code ..."
wget https://packagecloud.io/headmelted/codebuilds/gpgkey -O - | sudo apt-key add -
curl -L https://raw.githubusercontent.com/headmelted/codebuilds/master/docs/installers/apt.sh | sudo bash
