# Raspberry PI 4 stuff

## PiTFT stats display

On my rpi4 in a Argon Neo metal case, I'm using an [Adafruit Mini PiTFT 1.3"](https://learn.adafruit.com/adafruit-mini-pitft-135x240-color-tft-add-on-for-raspberry-pi/overview) display to show hostname, IP and some statistics.
The display shuts off after 30 seconds and can be re-activated by pressing the top left button. 

![rpi4_neo_stats](images/rpi4_neo_stats.jpg)

Pressing the bottom right button shuts down the raspberry pi, showing '...poweroff...' in the display after about 10 seconds. 
This is a result of never actually powering off the unit via the linux command 'poweroff', hence the display is powered again
automatically and displays the last text shown. It is now safe to unplug.

![rpi4_neo_poweroff](images/rpi4_neo_poweroff.jpg)

The script [minipitft_stats.py](minipitft_stats.py) is built based on the adafruit examples on github [rgb_display_minipitfttest.py](https://github.com/adafruit/Adafruit_CircuitPython_RGB_Display/blob/master/examples/rgb_display_minipitfttest.py) and [rgb_display_minipitftstats.py](https://github.com/adafruit/Adafruit_CircuitPython_RGB_Display/blob/master/examples/rgb_display_minipitftstats.py).

## Ethernet over USB (OTG)

Process described on my blog [thether rpi to ipad pro via ethernet over usb-c](https://marcelwiget.blog/2018/12/02/tether-rpi-to-ipad-pro-via-ethernet-over-usb-c/). But here in short:

After burning the microSD card, update the following files on /boot:

```
$ cat cmdline.txt
console=serial0,115200 console=tty1 root=PARTUUID=ea7d04d6-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait modules-load=dwc2,g_ether
```

```
$ tail -3 config.txt
[all]
#dtoverlay=vc4-fkms-v3d                                                    
dtoverlay=dwc2
```

and enable ssh!! By creating an empty ssh file in /boot:

```
$ touch ssh
```

Enable stats as a service on ubuntu:

```
$ sudo cp stats.service /etc/systemd/system/
$ sudo cp minipitft_stats.py /usr/local/sbin/
$ sudo systemctl start stats
$ sudo systemctl enable stats
```

