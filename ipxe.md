Burning ipxe onto 82599 intel nic

build image

```
$ clone https://git.ipxe.org/ipxe.git
$ cd ipxe/src
$ sudo apt install liblzma-dev
$ make bin/808610fb.rom
```

The hexfilename comes from the NIC id:

```
$  lspci -nn|grep 82599
06:00.0 Ethernet controller [0200]: Intel Corporation 82599ES 10-Gigabit SFI/SFP+ Network Connection [8086:10fb] (rev 01)
06:00.1 Ethernet controller [0200]: Intel Corporation 82599ES 10-Gigabit SFI/SFP+ Network Connection [8086:10fb] (rev 01)
```


read the flash size of the nic

```
$ sudo flashrom -p nicintel_spi:pci=06:00.0 --flash-size
flashrom v1.2 on Linux 5.4.0-51-generic (x86_64)
flashrom is free software, get the source code at https://flashrom.org

Using clock_gettime for delay loops (clk_id: 1, resolution: 1ns).
===
This PCI device is UNTESTED. Please report the 'flashrom -p xxxx' output
to flashrom@flashrom.org if it works for you. Please add the name of your
PCI device to the subject. Thank you for your help!
===
Found Micron/Numonyx/ST flash chip "M25P40" (512 kB, SPI) on nicintel_spi.
524288
```

expand size of the image

```
cat bin/808610fb.rom /dev/zero | dd bs=1024 count=512 of=bin/808610fb.rom.padded
```


```
$ sudo flashrom -p nicintel_spi:pci=06:00.0 -w bin/808610fb.rom.padded 
flashrom v1.2 on Linux 5.4.0-51-generic (x86_64)
flashrom is free software, get the source code at https://flashrom.org

Using clock_gettime for delay loops (clk_id: 1, resolution: 1ns).
===
This PCI device is UNTESTED. Please report the 'flashrom -p xxxx' output
to flashrom@flashrom.org if it works for you. Please add the name of your
PCI device to the subject. Thank you for your help!
===
Found Micron/Numonyx/ST flash chip "M25P40" (512 kB, SPI) on nicintel_spi.
Reading old flash chip contents... done.
Erasing and writing flash chip... Erase/write done.
Verifying flash... VERIFIED.
```

Use embedded script to custom load from gitlab:

https://ipxe.org/embed


```
make bin/undionly.kpxe EMBED=myscript.ipxe
```


example script:

```
#!ipxe
  
dhcp
chain http://bootserver/boot.php
```

dynamic script example:

```
http://192.168.0.1/boot.php?mac=${net0/mac}&asset=${asset:uristring}
```


auto-configure interface via ipv6:

ifconf --configurator ipv6 net0

or simply

```
ifconf -c ipv6
ifconf -c ipv4
```

```
iPXE> show uuid
  smbios/uuid:uuid = 86a9f546-d31b-4ba2-9bbb-303b61bcf8d1
```

```
#!ipxe

:retry
ifconf -c ipv6
ifconf -c ipv4
route
chain http://gitlab.com/mwiget/lab/-/raw/master/ipxe/${uuid:uristring}.php || 
chain http://gitlab.com/mwiget/lab/-/raw/master/ipxe/chain.php?uuid=${uuid:uristring} || 
goto retry


```
src$ git diff config/general.h
diff --git a/src/config/general.h b/src/config/general.h
index 0c99bcbb..f55ad9e1 100644
--- a/src/config/general.h
+++ b/src/config/general.h
@@ -35,7 +35,7 @@ FILE_LICENCE ( GPL2_OR_LATER_OR_UBDL );
  */
 
 #define        NET_PROTO_IPV4          /* IPv4 protocol */
-//#define NET_PROTO_IPV6       /* IPv6 protocol */
+#define NET_PROTO_IPV6 /* IPv6 protocol */
 #undef NET_PROTO_FCOE          /* Fibre Channel over Ethernet protocol */
 #define        NET_PROTO_STP           /* Spanning Tree protocol */
 #define        NET_PROTO_LACP          /* Link Aggregation control protocol */
mwiget@ryzen1:~/git/ipxe/src$ 
```
