# Fedora CoreOS with Tailscale

Needed a VM on ESXi with tailscale and few other apps installed. Ignition file
contains setting up a sudo enabled user account, installs tailscale, mosh and tmux.

Edit fc1.yaml and run make. It will output the base64 encoded, butane generated,
ignition file.

Download OVA and create a new VM in ESXi according to the documentation. 
Expand options and copy-paste the base64 output to the first line and specify
'base64' in the second:

![ignition](ignition.jpg)

The VM will launch automatically and you'll be able to log in with the username
specified in the fc1.yaml file. 

The installation of the packages will take a bit more time. Just check if tailscale
is available and if it is, bring it up and authenticate:

```
tailscale up
```

No reboot required.

