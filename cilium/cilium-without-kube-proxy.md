# Cilium lab trials

## Install requirements

Following the sandbox version using Kind, according to https://docs.cilium.io/en/v1.8/gettingstarted/kind/ .

Installing kubectl:

```
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
sudo mv kubectl /usr/local/bin/
```


```
$ kubectl version --client
Client Version: version.Info{Major:"1", Minor:"19", GitVersion:"v1.19.2", GitCommit:"f5743093fd1c663cb0cbc89748f730662345d44d", GitTreeState:"clean", BuildDate:"2020-09-16T13:41:02Z", GoVersion:"go1.15", Compiler:"gc", Platform:"linux/amd64"}
```

Install helm, following https://helm.sh/docs/intro/install/ :

```
$ https://get.helm.sh/helm-v3.3.3-linux-amd64.tar.gz
$ tar zxvf helm-v3.3.3-linux-amd64.tar.gz linux-amd64/helm
$ sudo mv linux-amd64/helm /usr/local/bin/
```

```
$ helm version
version.BuildInfo{Version:"v3.3.3", GitCommit:"55e3ca022e40fe200fbc855938995f40b2a68ce0", GitTreeState:"clean", GoVersion:"go1.14.9"}
```

Installing kind according to https://kind.sigs.k8s.io/#installation-and-usage , which requires go, which I installed using snap

```
$ sudo snap install go --classic
$ go get sigs.k8s.io/kind
```

Adding go/bin/ to my path in ~/.profile:

```
$ tail ~/.profile
PATH="$(go env GOPATH)/bin:$PATH"
```

Check version of kind:

```
$ kind version
kind v0.10.0-alpha go1.15.2 linux/amd64
```

## Bring up cluster

Still following the installation of cilium using kind, bring up a cluster with 3 worker nodes and 1 control-plane node.
Using custom subnet declaration in [kind-config.yaml](kind-config.yaml).

```
$ kind create cluster --config=kind-config.yaml
Creating cluster "kind" ...
 ✓ Ensuring node image (kindest/node:v1.19.1) 🖼 
 ✓ Preparing nodes 📦 📦 📦 📦  
 ✓ Writing configuration 📜 
 ✓ Starting control-plane 🕹️ 
 ✓ Installing StorageClass 💾 
 ✓ Joining worker nodes 🚜 
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Have a question, bug, or feature request? Let us know! https://kind.sigs.k8s.io/#community 🙂
```

Checking cluster nodes:

```
$ kubectl cluster-info --context kind-kind
KubeDNS is running at https://127.0.0.1:33993/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

Getting nodes, which are in 'NotReady' state until Cilium is deployed:

```
$ kubectl get nodes
NAME                 STATUS     ROLES    AGE     VERSION
kind-control-plane   NotReady   master   3m18s   v1.19.1
kind-worker          NotReady   <none>   2m45s   v1.19.1
kind-worker2         NotReady   <none>   2m45s   v1.19.1
kind-worker3         NotReady   <none>   2m45s   v1.19.1
```

These nodes run as docker containers:

```
$ docker ps
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS                 PORTS                       NAMES
6b746dd5d9f9        kindest/node:v1.19.1   "/usr/local/bin/entr…"   4 minutes ago       Up 4 minutes           127.0.0.1:33993->6443/tcp   kind-control-plane
668dafa58716        kindest/node:v1.19.1   "/usr/local/bin/entr…"   4 minutes ago       Up 4 minutes                                       kind-worker3
8a88b2e83fc1        kindest/node:v1.19.1   "/usr/local/bin/entr…"   4 minutes ago       Up 4 minutes                                       kind-worker
cdb54ebcfc5d        kindest/node:v1.19.1   "/usr/local/bin/entr…"   4 minutes ago       Up 4 minutes                                       kind-worker2
```

## remove kube-proxy

We want to run Cilium without kube-proxy, so we need to disable/remove kube-proxy.

```
$ kubectl get pods --all-namespaces
NAMESPACE            NAME                                         READY   STATUS    RESTARTS   AGE
kube-system          coredns-f9fd979d6-fgdn2                      0/1     Pending   0          2m26s
kube-system          coredns-f9fd979d6-lm8n7                      0/1     Pending   0          2m26s
kube-system          etcd-kind-control-plane                      1/1     Running   0          2m30s
kube-system          kube-apiserver-kind-control-plane            1/1     Running   0          2m30s
kube-system          kube-controller-manager-kind-control-plane   1/1     Running   0          2m31s
kube-system          kube-proxy-4hvh7                             1/1     Running   0          2m12s
kube-system          kube-proxy-7lhv8                             1/1     Running   0          2m12s
kube-system          kube-proxy-j6nj6                             1/1     Running   0          2m26s
kube-system          kube-proxy-tztd9                             1/1     Running   0          2m12s
kube-system          kube-scheduler-kind-control-plane            1/1     Running   0          2m31s
local-path-storage   local-path-provisioner-78776bfc44-zsvrp      0/1     Pending   0          2m26s
```

We see multiple kube-proxy pods running.

```
$ kubectl delete daemonsets -n kube-system kube-proxy  
```

And the kube-proxy pods are gone:

```
$ kubectl get pods --all-namespaces
NAMESPACE            NAME                                         READY   STATUS    RESTARTS   AGE
kube-system          coredns-f9fd979d6-fgdn2                      0/1     Pending   0          3m8s
kube-system          coredns-f9fd979d6-lm8n7                      0/1     Pending   0          3m8s
kube-system          etcd-kind-control-plane                      1/1     Running   0          3m12s
kube-system          kube-apiserver-kind-control-plane            1/1     Running   0          3m12s
kube-system          kube-controller-manager-kind-control-plane   1/1     Running   0          3m13s
kube-system          kube-scheduler-kind-control-plane            1/1     Running   0          3m13s
local-path-storage   local-path-provisioner-78776bfc44-zsvrp      0/1     Pending   0          3m8s
```

## install Cilium without kube-proxy

https://docs.cilium.io/en/stable/gettingstarted/kubeproxy-free/ describes the process. Basically 

```
$ kubectl cluster-info --context kind-kind 
Kubernetes master is running at https://127.0.0.1:42997
KubeDNS is running at https://127.0.0.1:42997/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

Check the internal IP's of the PODs. We need the API_SERVER_IP and API_SERVER_PORT to install Cilium without kube-proxy.
As we run the cluster using kind, the IP is the container's internal IP for the control-plane, which is 172.18.0.5 for this
deployment. 


```
$ kubectl get nodes -o wide
NAME                 STATUS     ROLES    AGE     VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE                                     KERNEL-VERSION     CONTAINER-RUNTIME
kind-control-plane   NotReady   master   5m32s   v1.19.1   172.18.0.5    <none>        Ubuntu Groovy Gorilla (development branch)   5.4.0-47-generic   containerd://1.4.0
kind-worker          NotReady   <none>   4m59s   v1.19.1   172.18.0.4    <none>        Ubuntu Groovy Gorilla (development branch)   5.4.0-47-generic   containerd://1.4.0
kind-worker2         NotReady   <none>   4m59s   v1.19.1   172.18.0.3    <none>        Ubuntu Groovy Gorilla (development branch)   5.4.0-47-generic   containerd://1.4.0
kind-worker3         NotReady   <none>   4m59s   v1.19.1   172.18.0.2    <none>        Ubuntu Groovy Gorilla (development branch)   5.4.0-47-generic   containerd://1.4.0
```

The API_SERVER_PORT is the default port 6443, which is exposed to the host as 42997. This can be seen in the ports column of docker ps:

```
$ docker ps
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS                  PORTS                       NAMES
97da07433046        kindest/node:v1.19.1   "/usr/local/bin/entr…"   11 minutes ago      Up 11 minutes                                       kind-worker
bc3340d519bd        kindest/node:v1.19.1   "/usr/local/bin/entr…"   11 minutes ago      Up 11 minutes                                       kind-worker2
52067d85b3f8        kindest/node:v1.19.1   "/usr/local/bin/entr…"   11 minutes ago      Up 11 minutes           127.0.0.1:42997->6443/tcp   kind-control-plane
eb5fccfe66af        kindest/node:v1.19.1   "/usr/local/bin/entr…"   11 minutes ago      Up 11 minutes                                       kind-worker3
```

Now lets bring up Cilium using the found IP and port 172.18.0.5 6443:

```
$ helm install cilium cilium/cilium --version 1.8.3 \
    --namespace kube-system \
    --set global.kubeProxyReplacement=strict \
    --set global.k8sServiceHost=172.18.0.5 \
    --set global.k8sServicePort=6443
NAME: cilium
LAST DEPLOYED: Tue Sep 22 12:15:15 2020
NAMESPACE: kube-system
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
You have successfully installed Cilium.

Your release version is 1.8.3.

For any further help, visit https://docs.cilium.io/en/v1.8/gettinghelp
```

Check process of install:

```
$ kubectl -n kube-system get pods 
NAME                                         READY   STATUS              RESTARTS   AGE
cilium-grz5j                                 0/1     Init:0/1            0          39s
cilium-nx2nf                                 0/1     Init:0/1            0          39s
cilium-operator-7b9779dfd8-5g6d9             0/1     ContainerCreating   0          39s
cilium-operator-7b9779dfd8-grvgb             0/1     ContainerCreating   0          39s
cilium-vklf4                                 0/1     Init:0/1            0          39s
cilium-xhzrt                                 0/1     Init:0/1            0          39s
coredns-f9fd979d6-fgdn2                      0/1     Pending             0          13m
coredns-f9fd979d6-lm8n7                      0/1     Pending             0          13m
etcd-kind-control-plane                      1/1     Running             0          13m
kube-apiserver-kind-control-plane            1/1     Running             0          13m
kube-controller-manager-kind-control-plane   1/1     Running             0          13m
kube-scheduler-kind-control-plane            1/1     Running             0          13m
```

and a minute later all is up and running (without kube-proxy!):

```
$ kubectl -n kube-system get pods 
NAME                                         READY   STATUS    RESTARTS   AGE
cilium-grz5j                                 0/1     Running   0          70s
cilium-nx2nf                                 0/1     Running   0          70s
cilium-operator-7b9779dfd8-5g6d9             1/1     Running   0          70s
cilium-operator-7b9779dfd8-grvgb             1/1     Running   0          70s
cilium-vklf4                                 0/1     Running   0          70s
cilium-xhzrt                                 1/1     Running   0          70s
coredns-f9fd979d6-fgdn2                      0/1     Running   0          14m
coredns-f9fd979d6-lm8n7                      0/1     Running   0          14m
etcd-kind-control-plane                      1/1     Running   0          14m
kube-apiserver-kind-control-plane            1/1     Running   0          14m
kube-controller-manager-kind-control-plane   1/1     Running   0          14m
kube-scheduler-kind-control-plane            1/1     Running   0          14m
```

If kernel is too old (not the case here, running ubuntu 20.04 with kernel 5.4.0-47-generic, cilium will not run:

```
$ kubectl -n kube-system get pods -l k8s-app=cilium
NAME           READY   STATUS    RESTARTS   AGE
cilium-grz5j   1/1     Running   0          5m44s
cilium-nx2nf   1/1     Running   0          5m44s
cilium-vklf4   1/1     Running   0          5m44s
cilium-xhzrt   1/1     Running   0          5m44s
```

Validate Cilium running in the desired mode:

```
$ kubectl exec -it -n kube-system cilium-grz5j -- cilium status | grep KubeProxyReplacement
KubeProxyReplacement:   Strict   [eth0 (DR)]   [NodePort (SNAT, 30000-32767, XDP: DISABLED), HostPort, ExternalIPs, HostReachableServices (TCP, UDP), SessionAffinity]
```

Still shows XDP being disabled. See below.



## Deploy the connectivity test

```
$ kubectl apply -f https://raw.githubusercontent.com/cilium/cilium/v1.8/examples/kubernetes/connectivity-check/connectivity-check.yaml
deployment.apps/echo-a created
deployment.apps/echo-b created
deployment.apps/echo-b-host created
deployment.apps/pod-to-a created
deployment.apps/pod-to-external-1111 created
deployment.apps/pod-to-a-denied-cnp created
deployment.apps/pod-to-a-allowed-cnp created
deployment.apps/pod-to-external-fqdn-allow-google-cnp created
deployment.apps/pod-to-b-multi-node-clusterip created
deployment.apps/pod-to-b-multi-node-headless created
deployment.apps/host-to-b-multi-node-clusterip created
deployment.apps/host-to-b-multi-node-headless created
deployment.apps/pod-to-b-multi-node-nodeport created
deployment.apps/pod-to-b-intra-node-nodeport created
service/echo-a created
service/echo-b created
service/echo-b-headless created
service/echo-b-host-headless created
ciliumnetworkpolicy.cilium.io/pod-to-a-denied-cnp created
ciliumnetworkpolicy.cilium.io/pod-to-a-allowed-cnp created
ciliumnetworkpolicy.cilium.io/pod-to-external-fqdn-allow-google-cnp created
```

check results: (the doc asks for cilium-test namespace, but in my case, its in default:

```
$ kubectl get namespaces
NAME                 STATUS   AGE
default              Active   17m
kube-node-lease      Active   17m
kube-public          Active   17m
kube-system          Active   17m
local-path-storage   Active   17m
```


```
$ kubectl get pods 
NAME                                                     READY   STATUS    RESTARTS   AGE
echo-a-7cfcbc66f-ljtzp                                   1/1     Running   0          78s
echo-b-869d875845-4gqtx                                  1/1     Running   0          78s
echo-b-host-ff5b64d5d-szm4w                              1/1     Running   0          78s
host-to-b-multi-node-clusterip-67548bfd68-hcpjs          1/1     Running   0          77s
host-to-b-multi-node-headless-55bc584bbb-ssrkl           1/1     Running   0          77s
pod-to-a-79444666cb-g5jqk                                1/1     Running   0          78s
pod-to-a-allowed-cnp-7f58895888-6gq78                    1/1     Running   0          78s
pod-to-a-denied-cnp-75cb89dfd-zmxf4                      1/1     Running   0          78s
pod-to-b-intra-node-nodeport-7dbdcff754-vhm5b            1/1     Running   0          76s
pod-to-b-multi-node-clusterip-5c949446c4-28mmj           1/1     Running   0          77s
pod-to-b-multi-node-headless-549ff7f798-qf8d8            1/1     Running   0          77s
pod-to-b-multi-node-nodeport-69d7c5df8-jvnb7             1/1     Running   0          76s
pod-to-external-1111-654646d47d-sk5rl                    1/1     Running   0          78s
pod-to-external-fqdn-allow-google-cnp-6557d6b7d6-f6zln   1/1     Running   0          78s
```

## Delete connectivity test:

```
$ kubectl delete -f https://raw.githubusercontent.com/cilium/cilium/v1.8/examples/kubernetes/connectivity-check/connectivity-check.yaml  
deployment.apps "echo-a" deleted
deployment.apps "echo-b" deleted
deployment.apps "echo-b-host" deleted
deployment.apps "pod-to-a" deleted
deployment.apps "pod-to-external-1111" deleted
deployment.apps "pod-to-a-denied-cnp" deleted
deployment.apps "pod-to-a-allowed-cnp" deleted
deployment.apps "pod-to-external-fqdn-allow-google-cnp" deleted
deployment.apps "pod-to-b-multi-node-clusterip" deleted
deployment.apps "pod-to-b-multi-node-headless" deleted
deployment.apps "host-to-b-multi-node-clusterip" deleted
deployment.apps "host-to-b-multi-node-headless" deleted
deployment.apps "pod-to-b-multi-node-nodeport" deleted
deployment.apps "pod-to-b-intra-node-nodeport" deleted
service "echo-a" deleted
service "echo-b" deleted
service "echo-b-headless" deleted
service "echo-b-host-headless" deleted
ciliumnetworkpolicy.cilium.io "pod-to-a-denied-cnp" deleted
ciliumnetworkpolicy.cilium.io "pod-to-a-allowed-cnp" deleted
ciliumnetworkpolicy.cilium.io "pod-to-external-fqdn-allow-google-cnp" deleted
```

## Deploy nginx pods

```
$ kubectl apply -f nginx-pods.yaml 
deployment.apps/my-nginx created

$ kubectl get pods
NAME                        READY   STATUS              RESTARTS   AGE
my-nginx-5b56ccd65f-9dl9p   0/1     ContainerCreating   0          4s
my-nginx-5b56ccd65f-njbx5   0/1     ContainerCreating   0          4s
```

And a few seconds later:

```
$ kubectl get pods
NAME                        READY   STATUS    RESTARTS   AGE
my-nginx-5b56ccd65f-9dl9p   1/1     Running   0          29s
my-nginx-5b56ccd65f-njbx5   1/1     Running   0          29s
```

With more details:

```
$  kubectl get pods -l run=my-nginx -o wide
NAME                        READY   STATUS    RESTARTS   AGE   IP           NODE           NOMINATED NODE   READINESS GATES
my-nginx-5b56ccd65f-9dl9p   1/1     Running   0          58s   10.0.3.213   kind-worker2   <none>           <none>
my-nginx-5b56ccd65f-njbx5   1/1     Running   0          58s   10.0.1.240   kind-worker3   <none>           <none>
```

In the next step, we create a NodePort service for the two instances:

```
$ kubectl expose deployment my-nginx --type=NodePort --port=80
service/my-nginx exposed
```

Verify the NodePort service got created:

```
$ kubectl get svc my-nginx
NAME       TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
my-nginx   NodePort   10.9.155.195   <none>        80:30143/TCP   16s
```

With the help of the cilium service list command, we can validate that Cilium’s BPF kube-proxy replacement created the new NodePort services under port 30143 (one per eth port):

```
$ kubectl get pods -n kube-system|grep cilium
cilium-grz5j                                 1/1     Running   0          14m
cilium-nx2nf                                 1/1     Running   0          14m
cilium-operator-7b9779dfd8-5g6d9             1/1     Running   0          14m
cilium-operator-7b9779dfd8-grvgb             1/1     Running   0          14m
cilium-vklf4                                 1/1     Running   0          14m
cilium-xhzrt                                 1/1     Running   0          14m
```

```
$ kubectl exec -it -n kube-system cilium-grz5j -- cilium service list
ID   Frontend           Service Type   Backend                
1    10.9.0.1:443       ClusterIP      1 => 172.18.0.5:6443   
2    10.9.0.10:53       ClusterIP      1 => 10.0.2.80:53      
                                       2 => 10.0.3.50:53      
3    10.9.0.10:9153     ClusterIP      1 => 10.0.2.80:9153    
                                       2 => 10.0.3.50:9153    
10   10.9.155.195:80    ClusterIP      1 => 10.0.3.213:80     
                                       2 => 10.0.1.240:80     
11   0.0.0.0:30143      NodePort       1 => 10.0.3.213:80     
                                       2 => 10.0.1.240:80     
12   172.18.0.4:30143   NodePort       1 => 10.0.3.213:80     
                                       2 => 10.0.1.240:80     

```

Trying to reach nginx via one of the worker nodes:

```
$ docker exec -ti kind-worker curl 172.18.0.4:30143
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

but still no XDP code on eth0 etc.
Looks like I need to follow this: https://docs.cilium.io/en/stable/gettingstarted/kubeproxy-free/#loadbalancer-nodeport-xdp-acceleration

