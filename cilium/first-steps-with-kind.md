# Cilium lab trials

## Install requirements

Following the sandbox version using Kind, according to https://docs.cilium.io/en/v1.8/gettingstarted/kind/ .

Installing kubectl:

```
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
sudo mv kubectl /usr/local/bin/
```


```
$ kubectl version --client
Client Version: version.Info{Major:"1", Minor:"19", GitVersion:"v1.19.2", GitCommit:"f5743093fd1c663cb0cbc89748f730662345d44d", GitTreeState:"clean", BuildDate:"2020-09-16T13:41:02Z", GoVersion:"go1.15", Compiler:"gc", Platform:"linux/amd64"}
```

Install helm, following https://helm.sh/docs/intro/install/ :

```
$ https://get.helm.sh/helm-v3.3.3-linux-amd64.tar.gz
$ tar zxvf helm-v3.3.3-linux-amd64.tar.gz linux-amd64/helm
$ sudo mv linux-amd64/helm /usr/local/bin/
```

```
$ helm version
version.BuildInfo{Version:"v3.3.3", GitCommit:"55e3ca022e40fe200fbc855938995f40b2a68ce0", GitTreeState:"clean", GoVersion:"go1.14.9"}
```

Installing kind according to https://kind.sigs.k8s.io/#installation-and-usage , which requires go, which I installed using snap

```
$ sudo snap install go --classic
$ go get sigs.k8s.io/kind
```

Adding go/bin/ to my path in ~/.profile:

```
$ tail ~/.profile
PATH="$(go env GOPATH)/bin:$PATH"
```

Check version of kind:

```
$ kind version
kind v0.10.0-alpha go1.15.2 linux/amd64
```

## Bring up cluster

Still following the installation of cilium using kind, bring up a cluster with 3 worker nodes and 1 control-plane node.
Using custom subnet declaration in [kind-config.yaml](kind-config.yaml).

```
$ kind create cluster --config=kind-config.yaml
Creating cluster "kind" ...
 ✓ Ensuring node image (kindest/node:v1.19.1) 🖼 
 ✓ Preparing nodes 📦 📦 📦 📦  
 ✓ Writing configuration 📜 
 ✓ Starting control-plane 🕹️ 
 ✓ Installing StorageClass 💾 
 ✓ Joining worker nodes 🚜 
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Have a question, bug, or feature request? Let us know! https://kind.sigs.k8s.io/#community 🙂
```

Checking cluster nodes:

```
$ kubectl cluster-info --context kind-kind
KubeDNS is running at https://127.0.0.1:33993/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

Getting nodes, which are in 'NotReady' state until Cilium is deployed:

```
$ kubectl get nodes
NAME                 STATUS     ROLES    AGE     VERSION
kind-control-plane   NotReady   master   3m18s   v1.19.1
kind-worker          NotReady   <none>   2m45s   v1.19.1
kind-worker2         NotReady   <none>   2m45s   v1.19.1
kind-worker3         NotReady   <none>   2m45s   v1.19.1
```

These nodes run as docker containers:

```
$ docker ps
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS                 PORTS                       NAMES
6b746dd5d9f9        kindest/node:v1.19.1   "/usr/local/bin/entr…"   4 minutes ago       Up 4 minutes           127.0.0.1:33993->6443/tcp   kind-control-plane
668dafa58716        kindest/node:v1.19.1   "/usr/local/bin/entr…"   4 minutes ago       Up 4 minutes                                       kind-worker3
8a88b2e83fc1        kindest/node:v1.19.1   "/usr/local/bin/entr…"   4 minutes ago       Up 4 minutes                                       kind-worker
cdb54ebcfc5d        kindest/node:v1.19.1   "/usr/local/bin/entr…"   4 minutes ago       Up 4 minutes                                       kind-worker2
```

## installing cilium

```
$ helm repo add cilium https://helm.cilium.io/
"cilium" has been added to your repositories
```

Preload cilium image into each worker node in the kind cluster. According to [https://helm.cilium.io/](https://helm.cilium.io/), v1.8.3 is the latest released one:

```
$ docker pull cilium/cilium:v1.8.3
$ kind load docker-image cilium/cilium:v1.8.3
```

Now install Cilium via Helm:

```
$ helm install cilium cilium/cilium --version 1.8.3 \
   --namespace kube-system \
   --set global.nodeinit.enabled=true \
   --set global.kubeProxyReplacement=partial \
   --set global.hostServices.enabled=false \
   --set global.externalIPs.enabled=true \
   --set global.nodePort.enabled=true \
   --set global.hostPort.enabled=true \
   --set config.bpfMasquerade=false \
   --set global.pullPolicy=IfNotPresent \
   --set config.ipam=kubernetes

NAME: cilium
LAST DEPLOYED: Mon Sep 21 10:44:53 2020
NAMESPACE: kube-system
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
You have successfully installed Cilium.

Your release version is 1.8.3.

For any further help, visit https://docs.cilium.io/en/v1.8/gettinghelp
```

Validate the installation:

```
$ $ kubectl -n kube-system get pods
NAME                                         READY   STATUS    RESTARTS   AGE
cilium-cm9fd                                 1/1     Running   0          50s
cilium-node-init-gbh4p                       1/1     Running   1          50s
cilium-node-init-lf554                       1/1     Running   1          50s
cilium-node-init-r5tvf                       1/1     Running   1          50s
cilium-node-init-vswsm                       1/1     Running   1          50s
cilium-nwkp4                                 1/1     Running   0          50s
cilium-operator-7f4dc846b6-2l7rh             1/1     Running   0          50s
cilium-operator-7f4dc846b6-472q5             1/1     Running   0          50s
cilium-tr24p                                 1/1     Running   0          50s
cilium-vzlm8                                 1/1     Running   0          50s
coredns-f9fd979d6-g2ncb                      1/1     Running   0          13m
coredns-f9fd979d6-tbvbm                      1/1     Running   0          13m
etcd-kind-control-plane                      1/1     Running   0          13m
kube-apiserver-kind-control-plane            1/1     Running   0          13m
kube-controller-manager-kind-control-plane   1/1     Running   0          13m
kube-proxy-68lrl                             1/1     Running   0          13m
kube-proxy-dfws6                             1/1     Running   0          13m
kube-proxy-t98jw                             1/1     Running   0          13m
kube-proxy-vbrbf                             1/1     Running   0          13m
kube-scheduler-kind-control-plane            1/1     Running   0          13m
```

## Deploy the connectivity test

```
$ kubectl apply -f https://raw.githubusercontent.com/cilium/cilium/v1.8/examples/kubernetes/connectivity-check/connectivity-check.yaml
deployment.apps/echo-a created
deployment.apps/echo-b created
deployment.apps/echo-b-host created
deployment.apps/pod-to-a created
deployment.apps/pod-to-external-1111 created
deployment.apps/pod-to-a-denied-cnp created
deployment.apps/pod-to-a-allowed-cnp created
deployment.apps/pod-to-external-fqdn-allow-google-cnp created
deployment.apps/pod-to-b-multi-node-clusterip created
deployment.apps/pod-to-b-multi-node-headless created
deployment.apps/host-to-b-multi-node-clusterip created
deployment.apps/host-to-b-multi-node-headless created
deployment.apps/pod-to-b-multi-node-nodeport created
deployment.apps/pod-to-b-intra-node-nodeport created
service/echo-a created
service/echo-b created
service/echo-b-headless created
service/echo-b-host-headless created
ciliumnetworkpolicy.cilium.io/pod-to-a-denied-cnp created
ciliumnetworkpolicy.cilium.io/pod-to-a-allowed-cnp created
ciliumnetworkpolicy.cilium.io/pod-to-external-fqdn-allow-google-cnp created
```

check results: (the doc asks for cilium-test namespace, but in my case, its in default:

```
$ kubectl get namespaces
NAME                 STATUS   AGE
default              Active   17m
kube-node-lease      Active   17m
kube-public          Active   17m
kube-system          Active   17m
local-path-storage   Active   17m
```


```
$ kubectl get pods 
NAME                                                     READY   STATUS    RESTARTS   AGE
echo-a-7cfcbc66f-ljtzp                                   1/1     Running   0          78s
echo-b-869d875845-4gqtx                                  1/1     Running   0          78s
echo-b-host-ff5b64d5d-szm4w                              1/1     Running   0          78s
host-to-b-multi-node-clusterip-67548bfd68-hcpjs          1/1     Running   0          77s
host-to-b-multi-node-headless-55bc584bbb-ssrkl           1/1     Running   0          77s
pod-to-a-79444666cb-g5jqk                                1/1     Running   0          78s
pod-to-a-allowed-cnp-7f58895888-6gq78                    1/1     Running   0          78s
pod-to-a-denied-cnp-75cb89dfd-zmxf4                      1/1     Running   0          78s
pod-to-b-intra-node-nodeport-7dbdcff754-vhm5b            1/1     Running   0          76s
pod-to-b-multi-node-clusterip-5c949446c4-28mmj           1/1     Running   0          77s
pod-to-b-multi-node-headless-549ff7f798-qf8d8            1/1     Running   0          77s
pod-to-b-multi-node-nodeport-69d7c5df8-jvnb7             1/1     Running   0          76s
pod-to-external-1111-654646d47d-sk5rl                    1/1     Running   0          78s
pod-to-external-fqdn-allow-google-cnp-6557d6b7d6-f6zln   1/1     Running   0          78s
```

## Enable Hubble

Hubble is a fully distributed networking and security observability platform for cloud native workloads.
It is built on top of Cilium and eBPF to enable deep visibility into the communication and behavior of 
services as well as the networking infrastructure in a completely transparent manner.

Using local mode, where Hubble listens on a Unix domain socket. Cilium uses namespace kube-system.

```
$ helm upgrade cilium cilium/cilium --version 1.8.3 \
   --namespace kube-system \
   --reuse-values \
   --set global.hubble.enabled=true \
   --set global.hubble.metrics.enabled="{dns,drop,tcp,flow,port-distribution,icmp,http}"

Release "cilium" has been upgraded. Happy Helming!
NAME: cilium
LAST DEPLOYED: Mon Sep 21 10:55:46 2020
NAMESPACE: kube-system
STATUS: deployed
REVISION: 2
TEST SUITE: None
NOTES:
You have successfully installed Cilium.

Your release version is 1.8.3.

For any further help, visit https://docs.cilium.io/en/v1.8/gettinghelp
```

Restart Cilium daemonset to allow Cilium agent to pick up the ConfigMap changes:

```
$ kubectl rollout restart -n kube-system ds/cilium
idaemonset.apps/cilium restarted
```

Pick one Cilium instance and validate that Hubble is properly configured ot listen on a UNIX domain
socket:

```
$ kubectl exec -n kube-system -t ds/cilium -- hubble observe
TIMESTAMP             SOURCE                                                        DESTINATION                                                   TYPE          VERDICT     SUMMARY
Sep 21 09:00:22.382   kube-system/coredns-f9fd979d6-g2ncb:53                        10.8.1.201:43092                                              to-overlay    FORWARDED   UDP
Sep 21 09:00:22.585   default/pod-to-b-multi-node-headless-549ff7f798-qf8d8:33496   kube-system/coredns-f9fd979d6-g2ncb:53                        to-endpoint   FORWARDED   UDP
Sep 21 09:00:22.585   default/pod-to-b-multi-node-headless-549ff7f798-qf8d8:33496   kube-system/coredns-f9fd979d6-g2ncb:53                        to-endpoint   FORWARDED   UDP
Sep 21 09:00:22.585   kube-system/coredns-f9fd979d6-g2ncb:53                        default/pod-to-b-multi-node-headless-549ff7f798-qf8d8:33496   to-overlay    FORWARDED   UDP
Sep 21 09:00:22.585   kube-system/coredns-f9fd979d6-g2ncb:53                        default/pod-to-b-multi-node-headless-549ff7f798-qf8d8:33496   to-overlay    FORWARDED   UDP
Sep 21 09:00:23.138   default/pod-to-a-allowed-cnp-7f58895888-6gq78:49716           kube-system/coredns-f9fd979d6-g2ncb:53                        to-endpoint   FORWARDED   UDP
Sep 21 09:00:23.138   default/pod-to-a-allowed-cnp-7f58895888-6gq78:49716           kube-system/coredns-f9fd979d6-g2ncb:53                        to-endpoint   FORWARDED   UDP
Sep 21 09:00:23.138   kube-system/coredns-f9fd979d6-g2ncb:53                        default/pod-to-a-allowed-cnp-7f58895888-6gq78:49716           to-overlay    FORWARDED   UDP
Sep 21 09:00:23.138   kube-system/coredns-f9fd979d6-g2ncb:53                        default/pod-to-a-allowed-cnp-7f58895888-6gq78:49716           to-overlay    FORWARDED   UDP
Sep 21 09:00:23.138   default/pod-to-a-allowed-cnp-7f58895888-6gq78:44376           default/echo-a-7cfcbc66f-ljtzp:8080                           to-endpoint   FORWARDED   TCP Flags: SYN
Sep 21 09:00:23.138   default/echo-a-7cfcbc66f-ljtzp:8080                           default/pod-to-a-allowed-cnp-7f58895888-6gq78:44376           to-overlay    FORWARDED   TCP Flags: SYN, ACK
Sep 21 09:00:23.138   default/pod-to-a-allowed-cnp-7f58895888-6gq78:44376           default/echo-a-7cfcbc66f-ljtzp:8080                           to-endpoint   FORWARDED   TCP Flags: ACK
Sep 21 09:00:23.138   default/pod-to-a-allowed-cnp-7f58895888-6gq78:44376           default/echo-a-7cfcbc66f-ljtzp:8080                           to-endpoint   FORWARDED   TCP Flags: ACK, PSH
Sep 21 09:00:23.141   default/echo-a-7cfcbc66f-ljtzp:8080                           default/pod-to-a-allowed-cnp-7f58895888-6gq78:44376           to-overlay    FORWARDED   TCP Flags: ACK, PSH
Sep 21 09:00:23.142   default/pod-to-a-allowed-cnp-7f58895888-6gq78:44376           default/echo-a-7cfcbc66f-ljtzp:8080                           to-endpoint   FORWARDED   TCP Flags: ACK, FIN
Sep 21 09:00:23.142   default/echo-a-7cfcbc66f-ljtzp:8080                           default/pod-to-a-allowed-cnp-7f58895888-6gq78:44376           to-overlay    FORWARDED   TCP Flags: ACK, FIN
Sep 21 09:00:23.142   default/pod-to-a-allowed-cnp-7f58895888-6gq78:44376           default/echo-a-7cfcbc66f-ljtzp:8080                           to-endpoint   FORWARDED   TCP Flags: ACK
Sep 21 09:00:23.522   default/pod-to-b-intra-node-nodeport-7dbdcff754-vhm5b:60730   kube-system/coredns-f9fd979d6-g2ncb:53                        to-endpoint   FORWARDED   UDP
Sep 21 09:00:23.522   default/pod-to-b-intra-node-nodeport-7dbdcff754-vhm5b:60730   kube-system/coredns-f9fd979d6-g2ncb:53                        to-endpoint   FORWARDED   UDP
Sep 21 09:00:23.522   kube-system/coredns-f9fd979d6-g2ncb:53                        default/pod-to-b-intra-node-nodeport-7dbdcff754-vhm5b:60730   to-overlay    FORWARDED   UDP
```

Finally, check if all nodes are now in ready state:

```
$ kubectl get nodes
NAME                 STATUS   ROLES    AGE   VERSION
kind-control-plane   Ready    master   31m   v1.19.1
kind-worker          Ready    <none>   31m   v1.19.1
kind-worker2         Ready    <none>   31m   v1.19.1
kind-worker3         Ready    <none>   31m   v1.19.1
```

## Deploy identity-aware and HTTP-aware Policy enforcement

According to https://docs.cilium.io/en/v1.8/gettingstarted/http/#gs-http

```
$ kubectl create -f https://raw.githubusercontent.com/cilium/cilium/v1.8/examples/minikube/http-sw-app.yaml
service/deathstar created
deployment.apps/deathstar created
pod/tiefighter created
pod/xwing created
```

This gives us the following pods and services (the connectivity tests are still running too):

```
$ kubectl get pods,svc
NAME                                                         READY   STATUS    RESTARTS   AGE
pod/deathstar-c74d84667-gjgdz                                1/1     Running   0          111s
pod/deathstar-c74d84667-zvbkn                                1/1     Running   0          111s
pod/echo-a-7cfcbc66f-ljtzp                                   1/1     Running   0          27m
pod/echo-b-869d875845-4gqtx                                  1/1     Running   0          27m
pod/echo-b-host-ff5b64d5d-szm4w                              1/1     Running   0          27m
pod/host-to-b-multi-node-clusterip-67548bfd68-hcpjs          1/1     Running   0          27m
pod/host-to-b-multi-node-headless-55bc584bbb-ssrkl           1/1     Running   0          27m
pod/pod-to-a-79444666cb-g5jqk                                1/1     Running   0          27m
pod/pod-to-a-allowed-cnp-7f58895888-6gq78                    1/1     Running   0          27m
pod/pod-to-a-denied-cnp-75cb89dfd-zmxf4                      1/1     Running   0          27m
pod/pod-to-b-intra-node-nodeport-7dbdcff754-vhm5b            1/1     Running   0          27m
pod/pod-to-b-multi-node-clusterip-5c949446c4-28mmj           1/1     Running   0          27m
pod/pod-to-b-multi-node-headless-549ff7f798-qf8d8            1/1     Running   0          27m
pod/pod-to-b-multi-node-nodeport-69d7c5df8-jvnb7             1/1     Running   0          27m
pod/pod-to-external-1111-654646d47d-sk5rl                    1/1     Running   0          27m
pod/pod-to-external-fqdn-allow-google-cnp-6557d6b7d6-f6zln   1/1     Running   0          27m
pod/tiefighter                                               1/1     Running   0          111s
pod/xwing                                                    1/1     Running   0          111s

NAME                           TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
service/deathstar              ClusterIP   10.9.204.55    <none>        80/TCP           111s
service/echo-a                 ClusterIP   10.9.253.34    <none>        8080/TCP         27m
service/echo-b                 NodePort    10.9.115.118   <none>        8080:31313/TCP   27m
service/echo-b-headless        ClusterIP   None           <none>        8080/TCP         27m
service/echo-b-host-headless   ClusterIP   None           <none>        <none>           27m
service/kubernetes             ClusterIP   10.9.0.1       <none>        443/TCP          43m
```

Each pod will be represented in Cilium as an Endpoint. We can invoke the cilium tool inside
the Cilium pod to list them:

```
$ kubectl -n kube-system get pods -l k8s-app=cilium
NAME           READY   STATUS    RESTARTS   AGE
cilium-6pg4s   1/1     Running   0          18m
cilium-8jmvw   1/1     Running   0          18m
cilium-mq7f8   1/1     Running   0          18m
cilium-xb5g6   1/1     Running   0          18m
```

```
$ kubectl -n kube-system exec cilium-6pg4s -- cilium endpoint list
ENDPOINT   POLICY (ingress)   POLICY (egress)   IDENTITY   LABELS (source:key[=value])          IPv6   IPv4         STATUS   
           ENFORCEMENT        ENFORCEMENT                                                                           
449        Disabled           Disabled          1          k8s:node-role.kubernetes.io/master                       ready   
                                                           reserved:host                                                    
2440       Disabled           Disabled          4          reserved:health                             10.8.0.181   ready   
```

and another one:

```
$ kubectl -n kube-system exec cilium-8jmvw -- cilium endpoint list
ENDPOINT   POLICY (ingress)   POLICY (egress)   IDENTITY   LABELS (source:key[=value])                       IPv6   IPv4         STATUS   
           ENFORCEMENT        ENFORCEMENT                                                                                        
343        Disabled           Disabled          43594      k8s:class=deathstar                                      10.8.2.93    ready   
                                                           k8s:io.cilium.k8s.policy.cluster=default                                      
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:org=empire                                                                
626        Disabled           Enabled           13047      k8s:io.cilium.k8s.policy.cluster=default                 10.8.2.159   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:name=pod-to-a-denied-cnp                                                  
804        Disabled           Disabled          10576      k8s:io.cilium.k8s.policy.cluster=default                 10.8.2.180   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:name=pod-to-b-multi-node-nodeport                                         
827        Disabled           Disabled          64304      k8s:io.cilium.k8s.policy.cluster=default                 10.8.2.226   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:name=pod-to-b-multi-node-headless                                         
1842       Disabled           Disabled          4          reserved:health                                          10.8.2.185   ready   
1996       Disabled           Disabled          10557      k8s:class=tiefighter                                     10.8.2.95    ready   
                                                           k8s:io.cilium.k8s.policy.cluster=default                                      
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:org=empire                                                                
2036       Disabled           Disabled          35321      k8s:io.cilium.k8s.policy.cluster=default                 10.8.2.194   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:name=pod-to-external-1111                                                 
2622       Disabled           Enabled           5357       k8s:io.cilium.k8s.policy.cluster=default                 10.8.2.130   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:name=pod-to-a-allowed-cnp                                                 
3833       Disabled           Disabled          1          reserved:host                                                         ready   
```

Both ingress and egress policy enforcement is still disabled in all of these pods because no network
policy has been imported yet.

From the perspective of the deathstar service, only the ships with label org=empire are allowed to connect
and request landing. Since we have no rules enforced, both xwing and tiefighter will be able to request landing:

```
$ kubectl exec xwing -- curl -s -XPOST deathstar.default.svc.cluster.local/v1/request-landing
Ship landed
$ kubectl exec tiefighter -- curl -s -XPOST deathstar.default.svc.cluster.local/v1/request-landing
Ship landed
```

Now create a policy to only allow trafic sent from any pods with label (org=empire),
file [sw_l3_l4_policy.yaml](sw_l3_l4_policy.yaml):

```
$ kubectl create -f sw_l3_l4_policy.yaml 
ciliumnetworkpolicy.cilium.io/rule1 created
```

Now run the landing requests again, tiefighter will succeed, but xwing shall fail:

```
$ kubectl exec tiefighter -- curl -s -XPOST deathstar.default.svc.cluster.local/v1/request-landing
Ship landed
$ kubectl exec xwing -- curl -s -XPOST deathstar.default.svc.cluster.local/v1/request-landing
(hangs, press ^C)
```

Inspecting the policies:

```
$ kubectl -n kube-system exec cilium-8jmvw -- cilium endpoint list
ENDPOINT   POLICY (ingress)   POLICY (egress)   IDENTITY   LABELS (source:key[=value])                       IPv6   IPv4         STATUS   
           ENFORCEMENT        ENFORCEMENT                                                                                        
343        Enabled            Disabled          43594      k8s:class=deathstar                                      10.8.2.93    ready   
                                                           k8s:io.cilium.k8s.policy.cluster=default                                      
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:org=empire                                                                
626        Disabled           Enabled           13047      k8s:io.cilium.k8s.policy.cluster=default                 10.8.2.159   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:name=pod-to-a-denied-cnp                                                  
804        Disabled           Disabled          10576      k8s:io.cilium.k8s.policy.cluster=default                 10.8.2.180   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:name=pod-to-b-multi-node-nodeport                                         
827        Disabled           Disabled          64304      k8s:io.cilium.k8s.policy.cluster=default                 10.8.2.226   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:name=pod-to-b-multi-node-headless                                         
1842       Disabled           Disabled          4          reserved:health                                          10.8.2.185   ready   
1996       Disabled           Disabled          10557      k8s:class=tiefighter                                     10.8.2.95    ready   
                                                           k8s:io.cilium.k8s.policy.cluster=default                                      
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:org=empire                                                                
2036       Disabled           Disabled          35321      k8s:io.cilium.k8s.policy.cluster=default                 10.8.2.194   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:name=pod-to-external-1111                                                 
2622       Disabled           Enabled           5357       k8s:io.cilium.k8s.policy.cluster=default                 10.8.2.130   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                               
                                                           k8s:io.kubernetes.pod.namespace=default                                       
                                                           k8s:name=pod-to-a-allowed-cnp                                                 
3833       Disabled           Disabled          1          reserved:host
```

```
$ kubectl -n kube-system exec cilium-xb5g6 -- cilium endpoint list
ENDPOINT   POLICY (ingress)   POLICY (egress)   IDENTITY   LABELS (source:key[=value])                                                      IPv6   IPv4         STATUS   
           ENFORCEMENT        ENFORCEMENT                                                                                                                       
40         Disabled           Enabled           51874      k8s:io.cilium.k8s.policy.cluster=default                                                10.8.1.168   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                                                              
                                                           k8s:io.kubernetes.pod.namespace=default                                                                      
                                                           k8s:name=pod-to-external-fqdn-allow-google-cnp                                                               
953        Disabled           Disabled          40145      k8s:app=local-path-provisioner                                                          10.8.1.68    ready   
                                                           k8s:io.cilium.k8s.policy.cluster=default                                                                     
                                                           k8s:io.cilium.k8s.policy.serviceaccount=local-path-provisioner-service-account                               
                                                           k8s:io.kubernetes.pod.namespace=local-path-storage                                                           
2036       Disabled           Disabled          1          reserved:host                                                                                        ready   
2144       Disabled           Disabled          24272      k8s:io.cilium.k8s.policy.cluster=default                                                10.8.1.100   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                                                              
                                                           k8s:io.kubernetes.pod.namespace=default                                                                      
                                                           k8s:name=echo-b                                                                                              
3062       Disabled           Disabled          4          reserved:health                                                                         10.8.1.24    ready   
3136       Disabled           Disabled          63041      k8s:io.cilium.k8s.policy.cluster=default                                                10.8.1.13    ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=coredns                                                              
                                                           k8s:io.kubernetes.pod.namespace=kube-system                                                                  
                                                           k8s:k8s-app=kube-dns                                                                                         
3662       Disabled           Disabled          54239      k8s:io.cilium.k8s.policy.cluster=default                                                10.8.1.208   ready   
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                                                              
                                                           k8s:io.kubernetes.pod.namespace=default                                                                      
                                                           k8s:name=pod-to-b-intra-node-nodeport                                                                        
3755       Enabled            Disabled          43594      k8s:class=deathstar                                                                     10.8.1.160   ready   
                                                           k8s:io.cilium.k8s.policy.cluster=default                                                                     
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                                                              
                                                           k8s:io.kubernetes.pod.namespace=default                                                                      
                                                           k8s:org=empire                                                                                               
3874       Disabled           Disabled          13874      k8s:class=xwing                                                                         10.8.1.4     ready   
                                                           k8s:io.cilium.k8s.policy.cluster=default                                                                     
                                                           k8s:io.cilium.k8s.policy.serviceaccount=default                                                              
                                                           k8s:io.kubernetes.pod.namespace=default                                                                      
                                                           k8s:org=alliance
```


Pods with label org=empire and class=deathstar now have ingress enforcement enabled as per the policy above.

Use kubectl to inspect the policy:

```
$ kubectl get cnp
NAME                                    AGE
pod-to-a-allowed-cnp                    56m
pod-to-a-denied-cnp                     56m
pod-to-external-fqdn-allow-google-cnp   56m
rule1                                   7m16s

$ kubectl describe cnp rule1
Name:         rule1
Namespace:    default
Labels:       <none>
Annotations:  <none>
API Version:  cilium.io/v2
Description:  L3-L4 policy to restrict deathstar access to empire ships only
Kind:         CiliumNetworkPolicy
Metadata:
  Creation Timestamp:  2020-09-21T09:36:29Z
  Generation:          1
  Managed Fields:
    API Version:  cilium.io/v2
    Fields Type:  FieldsV1
    fieldsV1:
      f:description:
      f:spec:
        .:
        f:endpointSelector:
          .:
          f:matchLabels:
            .:
            f:class:
            f:org:
        f:ingress:
    Manager:         kubectl-create
    Operation:       Update
    Time:            2020-09-21T09:36:29Z
  Resource Version:  14538
  Self Link:         /apis/cilium.io/v2/namespaces/default/ciliumnetworkpolicies/rule1
  UID:               96c466ef-69fc-49db-8ea1-6a46f76554ea
Spec:
  Endpoint Selector:
    Match Labels:
      Class:  deathstar
      Org:    empire
  Ingress:
    From Endpoints:
      Match Labels:
        Org:  empire
    To Ports:
      Ports:
        Port:      80
        Protocol:  TCP
Events:            <none>
```

## without kube-proxy

https://docs.cilium.io/en/stable/gettingstarted/kubeproxy-free/ describes how to set up cilium without kubeproxy. In my example
so far, kube-proxy is running and BPF code is attached to tc ingress. 

But I can't use kind, it only allows to select either iptables or ipvs for kube-proxy via their networking kubeProxyMode option. None doesn't exist.

Well, simply deleting the kube-proxy daemonset seems to work:

```
$ kubectl delete daemonsets -n kube-system kube-proxy
$ kubectl get pods --all-namespaces |grep proxy
```

kubectl get pods needs to be rerun a few times, until all proxy pods are removed (terminating state ...).


## cleanup

```
$ kind delete cluster
Deleting cluster "kind" ...
```

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                  PORTS                NAMES
```

done.
