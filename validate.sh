#!/bin/bash
for file in *.fcc; do
  target="${file%%.*}.ign"
  echo transpiling $file to $target ...
  docker run -i --rm quay.io/coreos/fcct:release --pretty --strict < $file > ignition/$target
done
