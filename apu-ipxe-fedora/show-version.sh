#!/bin/bash
user=mwiget
for apu in apu4a18 apu91dc apu49cc apu9290 apu4a7c; do
	echo -n "$apu: "
        ssh-keygen -f "/home/pi/.ssh/known_hosts" -R "$apu" >/dev/null 2>/dev/null
        ssh -q -o StrictHostKeyChecking=no $user@$apu "uname -a && echo -n \"$apu: kubeadm \" && kubeadm version -o short 2>/dev/null" || echo " --- not ready ---" 
done
