#!/bin/bash
user=mwiget
for apu in apu4a18 apu91dc apu49cc apu9290 apu4a7c; do
	echo wiping $apu ...
        ssh-keygen -f "/home/pi/.ssh/known_hosts" -R "$apu"
        ssh -q -o StrictHostKeyChecking=no $user@$apu sudo dd if=/dev/zero bs=1M of=/dev/sda count=5
        echo rebooting $apu ...
        ssh -q -o StrictHostKeyChecking=no $user@$apu sudo reboot
done
