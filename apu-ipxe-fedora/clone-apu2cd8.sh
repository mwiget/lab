#!/bin/bash
for apu in apu4a18 apu91dc apu49cc apu9290 apu4a7c; do
	echo "cloning html/apu2cd8.cfg to html/$apu.cfg and change hostname ..."
	cp html/apu2cd8.cfg html/$apu.cfg
	sed -i s/apu2cd8/$apu/ html/$apu.cfg
done
