
Follow https://www.puzzle.ch/de/blog/articles/2020/10/13/k3s-on-raspberry-pi

udpate cmdline.txt:

```
$ cat /boot/firmware/cmdline.txt
net.ifnames=0 dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=LABEL=writable rootfstype=ext4 elevator=deadline rootwait fixrtc cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory
```

install arkade, a hassle free way to get kubernetes apps and CLI

```
curl -sSL https://dl.get-arkade.dev | sudo sh
```

Enable ethernet over usb:

```
$ cat /boot/firmware/cmdline.txt 
net.ifnames=0 dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=LABEL=writable rootfstype=ext4 elevator=deadline rootwait fixrtc cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory modules-load=dwc2,g_ethermwiget@rigi:/boot/firmware

$ cat /boot/firmware/usercfg.txt 
# Place "config.txt" changes (dtparam, dtoverlay, disable_overscan, etc.) in
# this file. Please refer to the README file for a description of the various# configuration files on the boot partition.
dtoverlay=dwc2
```

Enable usb0 interface in netplan:

```
$ cat /etc/netplan/50-cloud-init.yaml
network:
  ethernets:
    eth0:
      dhcp4: true
      optional: true
    usb0:
      dhcp4: true
      optional: true
. . .
```


```


