# Gitlab-Runner on Fedora CoreOS

This repo contains a proof of concept to install Fedora CoreOS on baremetal server with a gitlab-runner using declarative configuration via 
[Fedora ignition](https://docs.fedoraproject.org/en-US/fedora-coreos/producing-ign/). The main idea is to bootstrap a toolserver up to and 
including a working gitlab-runner, ready to take on CICD jobs matching its tag(s). The full server configuration is stored on gitlab itself, 
except the secrets required for gitlab-runner to register with the gitlab server of choice. This reduces the "manual" work to the following steps:

1. Netboot server, which auto-installs and auto-provisions using PXE boot and an [ignition file](sm-config.fcc) from this repo
2. Upload a .env file containing the gitlab URL and runner token (CI_SERVER_URL and REGISTRATION_TOKEN): [gitlab-runner/push-and-restart-sm.sh](gitlab-runner/push-and-restart-sm.sh)
3. Restart systemctl service gitlab-runner or simply trigger a server reboot

A more detailed sequence diagram is shown here:

```mermaid
sequenceDiagram
  participant U as User
  participant S as Server
  participant D as DHCP Server
  participant T as TFTP Server
  participant H as HTTP Server
  participant G as GitLab
  U->>S: Trigger network boot (e.g. via ipmitool or redfish)
  S->>D: Request IP & boot image
  D->>S: Send IP & boot image
  S->>H: Request kernel & initrd
  H->>S: Send kernel & initrd
  S->>G: Request ignition config
  G->>S: Send ignition config
  U->>S: Upload runner credentials
  U->>S: Trigger service reload
```

The server is up and running using a fully declarative approach, with the users public key stored on gitlab. Only the secret gitlab runner registration
token must be uploaded by the user and the service/server restarted.

The .env file required to actually launch the gitlab-runner must contain the following two environment variables:

```
$ cat .env
CI_SERVER_URL=https://gitlab.com/
REGISTRATION_TOKEN=<secret_token>
```

This files must be uploaded after the server is running and have the gitlab-runner service restarted (or the whole server ;-)):

```
$ cat gitlab-runner/push-and-restart-sm.sh
#!/bin/bash
SERVER=192.168.0.53
echo "uploading .env file to register with gitlab.com/mwdca/fabric ..."
scp .env core@$SERVER:/etc/gitlab-runner/
echo "restart service gitlab-runner.service ..."
ssh core@$SERVER sudo systemctl restart gitlab-runner.service
```

To speed up PXE boot, an enhanced vefrsion of pxelinux, lpxelinux is used, which allows for http/https file transfer instead of the slower UDP based TFTP protocol.

```
/var/lib/tftpboot/lpxelinux.0
```

Extracted from alpine linux, which support http transfer in addition to tftp.

A shell script is used to compile (sorry, "transpile") the server configuration ignition files into JSON for server consumption straight 
from the ignition gitlab folder:

Transpile .fcc files via script validate.sh:

```
$ ./validate.sh 
transpiling ignition-config.fcc to ignition-config.ign ...
transpiling sm-config.fcc to sm-config.ign ...
transpiling st-config.fcc to st-config.ign ...
```

Generated files can be found in folder [ignition](ignition).

Fedora CoreOS version info used during this test installation:

```
[core@sm ~]$ uname -a
Linux sm.fritz.box 5.4.10-200.fc31.x86_64 #1 SMP Thu Jan 9 19:58:12 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux

[core@sm ~]$ cat /etc/os-release
NAME=Fedora
VERSION="31.20200118.3.0 (CoreOS)"
ID=fedora
VERSION_ID=31
VERSION_CODENAME=""
PLATFORM_ID="platform:f31"
PRETTY_NAME="Fedora CoreOS 31.20200118.3.0"
ANSI_COLOR="0;34"
LOGO=fedora-logo-icon
CPE_NAME="cpe:/o:fedoraproject:fedora:31"
HOME_URL="https://getfedora.org/coreos/"
DOCUMENTATION_URL="https://docs.fedoraproject.org/en-US/fedora-coreos/"
SUPPORT_URL="https://github.com/coreos/fedora-coreos-tracker/"
BUG_REPORT_URL="https://github.com/coreos/fedora-coreos-tracker/"
REDHAT_BUGZILLA_PRODUCT="Fedora"
REDHAT_BUGZILLA_PRODUCT_VERSION=31
REDHAT_SUPPORT_PRODUCT="Fedora"
REDHAT_SUPPORT_PRODUCT_VERSION=31
PRIVACY_POLICY_URL="https://fedoraproject.org/wiki/Legal:PrivacyPolicy"
VARIANT="CoreOS"
VARIANT_ID=coreos
OSTREE_VERSION='31.20200118.3.0'

[core@sm ~]$ rpm-ostree status
State: idle
AutomaticUpdates: disabled
Deployments:
● ostree://fedora:fedora/x86_64/coreos/stable
                   Version: 31.20200118.3.0 (2020-01-28T16:10:53Z)
                    Commit: 093f7da6ffa161ae1648a05be9c55f758258ab97b55c628bea5259f6ac6e370e
              GPGSignature: Valid signature by 7D22D5867F2A4236474BF7B850CB390B3C3359C4
```

Connected gitlab-runner show on server using docker:

```
[core@sm ~]$ docker exec -ti gitlab-runner gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=60 revision=003fe500 version=12.7.1
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
toolbox-dorfstrasse                                 Executor=docker Token=<hidden-token> URL=https://gitlab.com/
```

